import '../navbar/navbar.css';
import tentang from '../navbar/asset/tentang.png';
import tentang2 from '../navbar/asset/tentang2.png';
import bendera from '../navbar/asset/bendera.png';
import team1 from '../navbar/asset/team1.png';
import team2 from '../navbar/asset/team2.png';
import team3 from '../navbar/asset/team3.png';
import kapabilitas1 from '../navbar/asset/kapabilitas1.png';
import kapabilitas2 from '../navbar/asset/kapabilitas2.png';
import kapabilitas3 from '../navbar/asset/kapabilitas3.png';
import kapabilitas4 from '../navbar/asset/kapabilitas4.png';
import logokapabilitas from '../navbar/asset/logokapabilitas.png';
import ProfileTestimoni1 from '../navbar/asset/ProfileTesttimoni1.png';
import ProfileTestimoni2 from '../navbar/asset/ProfileTestimoni2.png';
import mitra1 from '../navbar/asset/mitra-1.png';
import mitra2 from '../navbar/asset/mitra-2.png';
import mitra3 from '../navbar/asset/mitra-3.png';
import mitra4 from '../navbar/asset/mitra-4.png';
import mitra5 from '../navbar/asset/mitra-5.png';
import mitra6 from '../navbar/asset/mitra-6.png';
import blog1 from '../navbar/asset/blog-1.png';
import blog2 from '../navbar/asset/blog-2.png';
import blog3 from '../navbar/asset/blog-3.png';
import blog4 from '../navbar/asset/blog-4.png';
import instagram from '../navbar/asset/instagram.png';
import linkedin from '../navbar/asset/linkedin.png';
import facebook from '../navbar/asset/facebook.png';
import twitter from '../navbar/asset/twitter.png';
import Beranda from '../navbar/asset/Beranda.png';
import Vector from '../navbar/asset/Vector.png';
import Left from '../navbar/asset/Left.png';
import Right from '../navbar/asset/Right.png';
import Date from '../navbar/asset/Date.png';
import garis from '../navbar/asset/garis.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faBars} from '@fortawesome/free-solid-svg-icons';

const Navbar = () => {
    return (
        <>
     <header>
        <section className="container">
        <div className="navbar">
            <div className="logo"><img src={logokapabilitas}/></div>
            <ul className="link">
                <li className="home"><a href="">HOME</a></li>
                <li><a href="">Tentang Kami</a></li>
                <li><a href="">Layanan</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Kontak Kami</a></li>
                <li><a href="">Lacak</a></li>
                <li><a href="">Lokasi</a></li>
                <li className="garis">|</li>
            </ul>
            <a href="" className="action_btn">INA <img src={bendera} alt="non"/></a>
             <div className="togle_btn">
                <a href=""><FontAwesomeIcon icon={faBars} /></a>
                <p className="p">sassa</p>
            </div>
        </div>
        {/* <div class="dropdown_menu">
            <li><a href="">HOME</a></li>
                <li><a href="">HOME</a></li>
                <li><a href="">HOME</a></li>
                <li><a href="">HOME</a></li>
                <li><a href="" class="action_btn">Get Started</a></li>
        </div> */}
        <div className="content">
        <div className="container-content">
            <h3>Solusi terpadu untuk semua skala bisnis</h3>
            <p>FLEXOFAST adalah pilihan tepat untuk bisnis Anda, dikelola oleh tim profesional dengan sumber daya lengkap dan canggih untuk mendukung rencana ekspansi bisnis Anda.</p>
            <a href="" className="btn">Mulai</a>
        </div>
    </div>
 </section>
</header>

<section className="tentang">
    <div className="section-tentang">
        <div className="garis">
            <h2><img src={garis} alt="" /> Tentang Kami</h2>
        </div>
        <div>
            <p className="satu">Raih peluang <span> fenomenal </span> </p>    
            <p className='dua'>di era digital</p>
        </div>
      </div>
      <div className="tentang-second">
        <p><span> FLEXOFAST </span> adalah pilihan tepat untuk bisnis Anda, <span>dikelola oleh tim profesional dengan sumber daya lengkap dan canggih </span>untuk mendukung <span className='tengah'> rencana ekspansi bisnis Anda.</span> </p>
    </div>
</section>

<section>
<div className="col">
    <div className="icon">
        <div className="icon-content">
            <img  src={tentang} alt=""/>
         {/* <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
          <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174"></path>
        </svg>  */}
    </div>
    <div>
    <h3>Gudang Kami</h3>
    <p>Cari tahu tentang gudang kami dan tentang pengembangan gudang kami</p>
    <a href="" className="btn-1">Baca Lengkap</a>
    <p className="p"></p>
    </div>
  </div>
    <div className="icon">
        <div className="icon-content">
            <img  src={tentang2} alt=""/>
        </div>  
        <div>
        <h3>Nilai Kami</h3>
        <p>Kami memiliki nilai dalam misi  untuk keberlanjutan perusahaan </p>
        <a href="" className="btn-1">Baca Lengkap</a>
        <p className="p"></p>
        </div>
        </div>
    <div className="icon">
        <div className="icon-content">
            <img className='gambar1' width="85" height="" src={team1} alt=""/>
            <img className='gambar2' width="85" height="" src={team2} alt=""/>
            <img className='gambar3' width="85" height="" src={team3} alt=""/>
    </div>
    <h3>Tim Kami</h3>
    <p>Kami memiliki tim yang berpengalaman di bidangnya masing-masing</p>
    <a href="" className="btn-1">Baca Lengkap</a>
    <p className="p"></p>
</div>
</div>
</section>


<section>
    <section className="kapabilitas">
        <div className="section-kapabilitas">
            <div className="garis">
                <h2><img src={garis} alt="" /> Kapabilitas 360</h2>
            </div>
            <div>
                <p className="satu">Optimalkan bisnis Anda <span> dengan solusi </span> </p>    
                <p>e-commerce yang inovatif dan mutakhir</p>
            </div>
          </div>
        </section>
        <div className="col-2">
            <div className="icon">
                <div className="kapabilitas-content">
                    <img  src={kapabilitas1} alt=""/>
            </div>
            {/* <img src={logokapabilitas} alt="" /> */}
            <p>Layanan Pemenuhan</p>
            <a href="" className="btn-2-1">Baca Lengkap -></a>
            <p className="p"></p>
            <div className='isi-icon '>
               {/* <p>lorem ankdaksndksnadadadjdbjabdkaankncnlc aicj cjascj j ocjo cpoCJopcj ac ic9c ccj asjs JJI jjioj ciajac jscja icj scjaiscjsjciascjs caisjc ascjia</p> */}
            </div>
          </div>
            <div className="icon">
                <div className="kapabilitas-content">
                    <img  src={kapabilitas2} alt=""/>
            </div>
            {/* <img src={logokapabilitas} alt="" /> */}
            <p>Manajemen E-Commerce </p>
            <a href="" className="btn-2">Baca Lengkap -></a>
            <p className="p"></p>
          </div>
            <div className="icon">
                <div className="kapabilitas-content">
                    <img width="200" height="200" src={kapabilitas3} alt=""/>
            </div>
            {/* <img src={logokapabilitas} alt="" /> */}
            <p>Pengiriman</p>
            <a href="" className="btn-2-3">Baca Lengkap -></a>
            <p className="p"></p>
          </div>
            <div className="icon">
                <div className="kapabilitas-content">
                    <img width="200" height="200" src={kapabilitas4} alt=""/>
            </div>
            {/* <img src={logokapabilitas} alt="" /> */}
            <p>Food Delivery</p>
            <a href="" className="btn-2-4">Baca Lengkap -> </a>
            <p className="p"></p>
          </div>
        </div>
</section>

<section className="testimoni">
        <div className="section-testimoni">
            <div className="garis">
                <h2><img src={garis} alt="" /> Testimmoni</h2>
            </div>
            <div>
                <p className="satu">Pendapat Mereka </p>    
            </div>
            {/* <!-- <p>kkk</p> --> */}
          </div>
          <div className="testimoni-second">
            <div className="bulet"><img src={Left} alt="" /></div>
            {/* <!-- <div class="bulet"></div> --> */}
        </div>
        <div className="kota-2"><img src={Right} alt="" /></div>
        </section>
        <section>
        <div className="col-3">
            <div className="icon">
                <div className="testimoni-content">
                </div>
                <img width="400" height="200" src={ProfileTestimoni1} alt=""/>
                <div className="testimoni-flex">
                    <div className="garis"></div>
                    <h3>Wagiyanto</h3>
                </div>
            <h4>AVP Deca Group (Whitelab)</h4>
            <p>FLEXOFAST memberikan pelayanan terbaik untuk penanganan operasional kami, layanan yang diberikan sangat profesional & fleksibel sesuai dengan kebutuhan operasional yang cukup kompleks sehingga kebutuhan kami dapat terpenuhi dengan baik.</p>
            <a href="" className="btn-2">“ <span>”</span></a>
            <p className="p"></p>
          </div>
            <div className="icon-2">
                <div className="testimoni-content">
                </div>
                <img width="250" height="170" src={ProfileTestimoni2} alt=""/>
                <div className="testimoni-flex">
                    <div className="garis"></div>
                    <h3>Melisa Oktaviani</h3>
                </div>
                <h4>Personal Care Team Manager (Cetaphil)</h4>
                <p>Sejak Eblo menggunakan layanan Flexofast, pesanan kami terisi dengan baik dan pengiriman lebih efisien. Produk kami tidak ada minus dan packaging sangat rapi. Dengan tim Flexofast yang maksimal dalam menangani brand kami, pelanggan sangat puas karena proses pengiriman yang sangat cepat.</p>
            <a href="" className="btn-2">“ <span>”</span></a>
            <p className="p"></p>
          </div>
            <div className="icon-3">
                <div className="testimoni-content">
                </div>
                <img width="250" height="170" src={ProfileTestimoni2} alt=""/>
                <div className="testimoni-flex">
                    <div className="garis"></div>
                    <h3>kkk</h3>
                </div>
                <h4>wkwk</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident reprehenderit ipsam accusamus architecto, delectus autem facere quam dolores veritatis itaque eligendi. Similique voluptatem officiis sed, dicta debitis aliquid impedit commodi?</p>
            <a href="" className="btn-2">“ <span>”</span></a>
            <p className="p"></p>
          </div>
          </div>
        </section>

        <div className="lihat-lengkap">
            <div className="isi-lihat-legkap">
                <button>Lihat Lengkap</button>
            </div>
        </div>
        
        <section>
            <div className="mitra">
                <div className="section-mitra">
                    {/* <!-- <div class="garis"></div> --> */}
                    <h2><img src={garis} alt="" /> Mitra</h2>
                 </div>  
                    <div className="center">
                        <p className="satu">Perusahaan yang sudah  </p>    
                        <p>bermitra bersama kami</p>
                    </div>
                  {/* <!-- </div> --> */}
            </div>
        </section>
        <div class="kerja-sama">
            <img class="img-1" src={mitra1}  alt=""/>
            <img src={mitra2} alt="ok"/>
            <img src={mitra3}  alt="ok"/>
            <img src={mitra4}  alt="ok"/>
            <img src={mitra5}  alt="ok"/>
            <img src={mitra6}  alt="oo"/>
        </div>


        <div className="terbaik">
                <div className="section-terbaik">
                    <div className="garis">
                        <h2><img src={garis} alt="" /> Bawa Yang Terbaik</h2>
                    </div>
                    <div>
                        <p className="satu">Pencapaian Kami <span>dalam</span></p>    
                        <p>36 bulan terakhir</p>
                    </div>
                  </div>
                  <div>
                   {/* <img src={blog1} heiht="500" alt="" /> */}
                </div>
            </div>
            <div className="terbaik-dua">
                <p>Berbagai pencapaian kami dalam 36 bulan terakhir menjadi salah <span> satu hal penting bagi kami</span></p>
                <button>Baca Lengkap</button>
            </div>
            <div className='tao'>
            <div className="terbaik-col">
               <div className="icon-terbaik">
                <p className='bold'>IDR 2 T</p>
                <p className="p">GMV Processed</p>
               </div>
               <div className="icon-terbaik">
                <p className='bold'>20 Milion</p>
                <p className="p">Package Handled</p>
               </div>
               
            </div>
            <div className="terbaik-col">
               <div className="icon-terbaik">
                <p className='bold'>15</p>
                <p className="p">Last mile courier partner</p>
               </div>
               <div className="icon-terbaik">
                <p className='bold'>25</p>
                <p className="p">E-commerce Integration</p>
               </div>
            </div>
            <div className="terbaik-col">
               <div className="icon-terbaik">
                <p className='bold'>3 Milion</p>
                <p className="p">Unique SKU</p>
               </div>
               <div className="icon-terbaik">
                <p className='bold'>99%</p>
                <p className="p">SLA Commitment</p>
               </div>
            </div>
            </div>
            <div>
            <div className='atas'>   
            <div className='pingir'>
            <img src={blog1}  alt="" />
            <img src={blog1} className='img-2' alt="" />
            <div className='garis'></div>
            <div className='garis-2'></div>
            </div>
            </div></div>

            <section>
                <section className="blog-kami">
                    <div className="section-blog-kami">
                        <div className="garis">
                            <h2><img src={garis} alt="" /> Blog Kami</h2>
                        </div>
                        <div>
                            <p className="satu">Berita dari kami </p>    
                        </div>
                        {/* <!-- <p>kkk</p> --> */}
                      </div>
                      <div className="blog-kami-second">
                        <div className="bulet"><img src={Left} alt="" /></div>
                        {/* <!-- <div class="bulet"></div> --> */}
                    </div>
                    <div className="bulet-2"><img src={Right} alt="" /></div>
                    </section>
                    <div className="col-4">
                        <div className="icon">
                            <div className="blog-kami-content">
                                <img width="250" height="210" src={blog1} alt=""/>
                        </div>
                        <h3>4 Online Business Problems
                             That Fulfillment Services</h3>
                              <p><span className="kalender"> <img src={Date} alt="" /></span> <span className="tanggal">21 March 2022</span> <span className="titik">.</span> <span className="type">Bussiness</span></p>
                        {/* <!-- <p>Lorem ispsum dolorlabore ex,!</p> -->
                        <!-- <a href="" className="btn-2">Mulai</a>
                        <p class="p"></p> --> */}
                      </div>
                        <div className="icon">
                            <div className="blog-kami-content">
                                <img width="250" height="210"  src={blog2} alt=""/>
                        </div>
                        <h3>Fulfillment Models in E-commerce Business</h3>
                        <p><span className="kalender"> <img src={Date} alt="" /></span> <span className="tanggal">21 March 2022</span> <span className="titik">.</span> <span className="type">Bussiness</span></p>
                        {/* <!-- <p>Lorem ipsum dolor abore ex,!</p> */}
                        {/* <a href="" className="btn-2">Mulai</a>
                        <p className="p"></p> --> */}
                      </div>
                        <div className="icon">
                            <div className="blog-kami-content">
                                <img width="250" height="210"  src={blog3} alt=""/>
                        </div>
                        <h3>Guaranteed Anti Complaints, a Fulfillment Service Trick</h3>
                        <p><span className="kalender"> <img src={Date} alt="" /></span> <span className="tanggal">21 March 2022</span> <span className="titik">.</span> <span className="type">Bussiness</span></p>
                        {/* <!-- <p>Lorem ipsum dolor labore ex,!</p>
                        <a href="" className="btn-2">Mulai </a>
                        <p className="p"></p> --> */}
                      </div>
                        <div className="icon">
                            <div className="blog-kami-content">
                                <img width="250" height="210"  src={blog4} alt=""/>
                        </div>
                        <h3>How Important is Warehouse in Fulfillment Services</h3>
                        <p><span className="kalender"> <img src={Date} alt="" /></span> <span className="tanggal">21 March 2022</span> <span className="titik">.</span> <span className="type">Bussiness</span></p>
                        {/* <!-- <a href="" class="btn-2">Mulai</a>
                        <p className="p"></p> --> */}
                      </div>
                    </div>
            </section>

            <div className="lihat-lengkap">
                <div className="isi-lihat-legkap">
                    <button>Lihat Lengkap</button>
                </div>
            </div>

            <section className="background">
                <div className="color">
                    <div className="isi">
                    <p>Optimalkan bisnis
                     online Anda sekarang!</p>
                     <div className="hubungi-kami">
                        <div className="isi-lihat-legkap">
                            <button>Hubungi Kami</button>
                        </div>
                    </div>
                    </div>
                    <div>
                   <img className="img-1" src={kapabilitas2} height="230" width="150" alt=""/>
                   <img className="img-2" src={blog2}height="230" width="150" alt=""/>
                </div>
                </div>
            </section>

            <section>
                <div className="background-end">
                    <div className="content-end">
                        <div className="isi">
                        <img className='img' src={logokapabilitas} alt="" />
                        <p><span><img src={Beranda} alt="" /></span> Jl. Pembangunan No. 9, Tangga Asem <span className='span-2'> Sewan,
                             Neglasari Tangerang, Indonesia</span></p>
                        <p><span><img src={Vector} alt="" /></span> + 62 858 9413 2085</p>
                    </div>
                    <div className="isi-2">
                       <ul className='ul'>
                        <li>Tentang Kami</li>
                        <li>Layanan</li>
                        <li>Blog</li>
                        <li>Kontak Kami</li>
                        <li>Lacak</li>
                        <li>Lokasi</li>
                       </ul>
                       <p>Copyright © 2021 PT Flexo Solusi Indonesia, All Rights Reserved</p>
                       <ul className="ulti">
                        <li><img src={instagram} alt="" /></li>
                        <li><img src={linkedin} alt="" /></li>
                        <li><img src={facebook} alt="" /></li>
                        <li><img src={twitter} alt="" /></li>
                        {/* <li>ig</li> */}
                       </ul>
                    </div>
                    </div>
                </div>
            </section>










    </>
    )
    
}

export default Navbar;